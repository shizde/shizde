# Shizde | Rafael S. Perez


<div style="display: inline_block"><br>
  <img align="center" alt="vim-logo" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vim/vim-original.svg">
  <img align="center" alt="python-logo" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
  <img align="center" alt="conda-logo" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/anaconda/anaconda-original.svg">
  <img align="center" alt="flask-logo" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/flask/flask-original.svg">
  <img align="center" alt="c-logo" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg">
  <img align="center" alt="cpp-logo" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg">
  <img align="center" alt="rust-logo" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/rust/rust-original.svg">
  <img align="center" alt="arduino-logo" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/arduino/arduino-original.svg">
  <img align="center" alt="lua-logo" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/lua/lua-original.svg">
</div>

<br>

Personal Blog: [Notebook Website](https://shizde.gitlab.io/shizde/)

- [Embedded Systems](#embedded-systems)
- [Compression Algorithms](#compression-algorithms)
- [Data Science](#data-science)
- [Python Back End](#python-back-end)
- [Rust Back End](#rust-back-end)
- [Data Analysis](#data-analysis )
- [Structured Documentation](#structured-documentation)
- [Simple Python Projects](#simple-python-projects)

## Embedded Systems

- [Lossless GNSS Schema]()[^1]


## Compression Algorithms

- [Arduino Huffman](https://gitlab.com/shizde/arduinohuffman) [^2]


## Data Science

- [Training Model](https://gitlab.com/shizde/house-pricing)


## Python Back End

 - [Flask App Example](https://gitlab.com/shizde/flaskapp)
 - [Flask API Example](https://gitlab.com/shizde/api-flask-example)
 - [WebFlask Portal Example](https://gitlab.com/shizde/webflask)
 - [Python Api Studies](https://gitlab.com/shizde/flask-rest-api)
 - [Yet Another Flask](https://gitlab.com/shizde/yet-another-flask)
 - [Plotly Studies](https://gitlab.com/shizde/dash-plotly-project)
 - [Streamlit Tests](https://gitlab.com/shizde/streamlit-tests)


# Rust Back End

- [Rust Microservices](https://gitlab.com/shizde/rust-microservice)
- [Rust Embedded](https://gitlab.com/shizde/rustesp)
- [Rust Base Layout](https://gitlab.com/shizde/rust-base-layout)


## Data Analysis 

- [Elections Analysis](https://gitlab.com/shizde/analise-eleicoes)


## Simple Projects

- [Alarm Clock in Python](https://gitlab.com/shizde/alarm-clock)
- [Mastermind Game in Python](https://gitlab.com/shizde/mastermind-project)
- [Password Generator in Python](https://gitlab.com/shizde/password-generator)
- [Path Finder in Python](https://gitlab.com/shizde/path-finder)
- [PyTorch Course](https://gitlab.com/shizde/pytorch-course)
- [Python OOP Tests](https://gitlab.com/shizde/python-oop-example)
- [Python ADV Tets](https://gitlab.com/shizde/pyadv)


### References
[^1]:[Lossless Compression Scheme for Efficient GNSS Data Transmission on IoT Devices](https://ieeexplore.ieee.org/document/9698642) 
[^2]:[µJSON, a Lightweight Compression Scheme for Embedded GNSS Data Transmission on IoT Nodes](https://ieeexplore.ieee.org/document/9766635) 
