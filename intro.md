Home
============


|                          |              |
|--------------------------|--------------|
|Rafael Stauffer Perez     |              |
|Porto                     | Portugal     |
|rafaelperez.dev@gmail.com | @perezrafael |


**Education**
---------

### 2022-2024 (expected) - **Porto University** (Porto/PT)
>**MSc, Computer Science**

**Thesis title: TBD**

### 2017-2021 - **Polytechnic Institute of Portalegre** (Portalegre/PT)
>**BSc, Computer Engineering**

**Minor: Embedded Systems**


### 2012-2017 (Incomplete) - **University of the State of Rio de Janeiro - UERJ** (Rio de Janeiro/BR)
>**BSc, Electrical Engineering**

**Minor: Telecommunications**

<br>

**Experience**
----------

### **Current Job**

Currently working at BNP Paribas Cardif at  Cardif Support in Portugal.
Data Science and Analytics Team.
Associate Software Engineer.

* Manage Data Analisys Platform

* Structure Platform Data's Reports

* Develop On Demand Projects


<br>


**Technical Experience**
--------------------

My Cool Side Project
:   Currently working on Compression Algorithms for Embedded Systems

    - Lossless Compression Algorithm
    - mJSON


#### Programming Languages   
- Python
- C / C++
- Rust
- Lua

#### Technologies   
- SQL        
- HQL
- MongoDB
- Postgre
- Flask
- Django
- PySpark
- Matlab

<br>

**Other Skills & Information**
----------------------------------------

* Human Languages:

     * Portuguese : native speaker
     * Spanish    : fluent
     * English    : fluent
     * French     : intermediate




>The economic anarchy of capitalist society as it exists today is, in my opinion, the real source of the evil. ― _Albert Einstein_

>Listen, strange women lying in ponds distributing swords is no basis for a system of government. Supreme executive power derives from a mandate from the masses, not from some farcical aquatic ceremony. ― _Monty Python And The Holy Grail_

<!-- 
:::{note}
Here is a note!
:::

And here is a code block:

```
e = mc^2
```
-->
* Botafogo F.R. lunatic fan
<img title="botafogo" alt="Botafogo" src="./static/img/botafogo.jpg">

