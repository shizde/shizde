# Sports

This is a brief topic about me and the sports that I love, the ones that I enjoy and the ones that were a part of my lifetime.

### Botafogo F.R.

First of all, talking about my religion: [Botafogo de Futebol e Regatas](https://botafogo.com.br). I don't have a religion, per say. I believe in doing good to everyone around, spreading love and good toughts about our mission in this world. Most of this I learn from Botafogo biggest idol: [Nilton Santos](https://pt.wikipedia.org/wiki/N%C3%ADlton_Santos). There's nothing I've been more passionate about my whole life.

### Handball

Despite loving football, I always loved to play handball. Everything that I love about football will go though what I love about Botafogo so there is always a partial side that I would take. 

### Tennis

One of the sports that I respect the most. Despite thinking that it's really aimed to entertain the royalty and those who owns the means of productions, I see a lot of beauty in tennis. It is a sport that allows you to have a great opportunity in every part of the game. There are no impossible turnbacks. Every set is a journey.

### Formule 1

A sport that has been designed to the rich but it was (partially) stolen by the poors in **Brasil**. The importance of Ayrton Senna in the sport and along the brazilian society of its time is something that I find majestic. It was designed by the rich, owned by the rich, aimed to the rich, had only riches as drivers but still all the society would be captivate by those cars running at 200Km/h. The images of brazilian crowd invading the running track to celebrate Ayrton Senna's victory is burned into my mind and I appreciate that it is.