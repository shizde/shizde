# Articles Published

## Lossless Compression Scheme for Efficient GNSS Data Transmission on IoT Devices

<div style="text-align: justify"> 

**Abstract**:
Wireless data transmission is one of the most energy-consuming tasks performed on embedded devices, being a crucial feature of battery-powered Internet of Things (IoT) applications. The present work proposes a new methodology to reduce the energy footprint of GNSS-based sensors by decreasing the amount of transmitted data applying a lossless compression strategy. Online trajectory data is structured through a pre-processing stage without information loss and posteriorly com-pressed through standard lossless algorithms. Simulations are performed considering different trajectory shapes, comparing the proposed schema with traditional compression methods without the proposed pre-processing stage. The results show that the proposed scheme can reach lower compression rates, reducing embedded IoT devices' energy footprint.

</div>

[IEEE Article](https://ieeexplore.ieee.org/document/9698642)
<!-- {cite}`lossless_gnss` -->

<br>

## µJSON, a Lightweight Compression Scheme for Embedded GNSS Data Transmission on IoT Nodes

<div style="text-align: justify"> 

Abstract:
Embedded computing is a sector in strong growth, driven by the increasing offer in the internet of things. Since position information is typically one of the intrinsic features of a sensory platform, the present work considers and analyzes the embedded lossless compression of localization data. A structural data scheme is proposed based on the number of occurrences of the independent coordinate components, named as µJSON. It is shown that, when considering a low-level language implementation on an embedded processor acquiring GNSS data, the proposed schema implies lower compression rates than its counterparts. An embedded test-bench platform is assembled, and several real case scenarios are considered for effectiveness and validation purposes.

</div>

[IEEE Article](https://ieeexplore.ieee.org/abstract/document/9766635)


<!-- {cite}`mJson` -->

